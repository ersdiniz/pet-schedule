package com.sippulse.pet.security;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class EncryptorTest {

    @Test
    public void testEncryptorNotEqualsToken() throws Exception {
        assertNotEquals(Encryptor.encrypt("a"), Encryptor.encrypt("a"));
    }
}