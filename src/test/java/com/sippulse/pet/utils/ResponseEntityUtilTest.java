package com.sippulse.pet.utils;

import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.Test;
import org.springframework.http.HttpStatus;

public class ResponseEntityUtilTest {

    @Test
    public void testStatusCreated() throws Exception {
        assertEquals(HttpStatus.CREATED, ResponseEntityUtil.created(new Object()).getStatusCode());
    }

    @Test
    public void testStatusOk() throws Exception {
        assertEquals(HttpStatus.OK, ResponseEntityUtil.okOrNoContent(new Object()).getStatusCode());
        assertEquals(HttpStatus.OK, ResponseEntityUtil.okOrNoContent(Collections.singletonList(new Object())).getStatusCode());
        assertEquals(HttpStatus.OK, ResponseEntityUtil.okOrNoContent(new Object[1]).getStatusCode());
    }

    @Test
    public void testStatusNoContent() throws Exception {
        assertEquals(HttpStatus.NO_CONTENT, ResponseEntityUtil.okOrNoContent(null).getStatusCode());
        assertEquals(HttpStatus.NO_CONTENT, ResponseEntityUtil.okOrNoContent(Collections.emptyList()).getStatusCode());
        assertEquals(HttpStatus.NO_CONTENT, ResponseEntityUtil.okOrNoContent(new Object[0]).getStatusCode());
    }
}