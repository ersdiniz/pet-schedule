package com.sippulse.pet.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.sippulse.pet.enums.TypeEmployee;

public class EmployeeDtoTest {

    @Test
    public void testBuilder() {
        final String name = "Jon";
        final String cpf = "12312312312";
        final TypeEmployee type = TypeEmployee.VET;
        final String username = "username";
        final String password = "password";

        final EmployeeDto dto = EmployeeDto.Builder.create()
                .name(name)
                .cpf(cpf)
                .type(type)
                .username(username)
                .password(password)
                .build();

        assertEquals(name, dto.getName());
        assertEquals(cpf, dto.getCpf());
        assertEquals(type, dto.getType());
        assertEquals(username, dto.getUsername());
        assertEquals(password, dto.getPassword());
    }
}