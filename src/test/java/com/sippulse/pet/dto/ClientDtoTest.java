package com.sippulse.pet.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ClientDtoTest {

    @Test
    public void testBuilder() {
        final String name = "Jon";
        final String cpf = "12312312312";

        final ClientDto dto = ClientDto.Builder.create()
                .name(name)
                .cpf(cpf)
                .build();

        assertEquals(name, dto.getName());
        assertEquals(cpf, dto.getCpf());
        assertTrue(dto.getPets().isEmpty());
    }
}