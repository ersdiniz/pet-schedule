package com.sippulse.pet.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PetDtoTest {

    @Test
    public void testBuilder() {
        final String name = "Rex";
        final String description = "Dog";
        final Long clientId = 3L;

        final PetDto dto = PetDto.Builder.create()
                .name(name)
                .description(description)
                .clientId(clientId)
                .build();

        assertEquals(name, dto.getName());
        assertEquals(description, dto.getDescription());
        assertEquals(clientId, dto.getClientId());
    }
}