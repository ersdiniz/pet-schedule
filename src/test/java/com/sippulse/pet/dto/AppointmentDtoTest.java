package com.sippulse.pet.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;

import org.junit.Test;

public class AppointmentDtoTest {

    @Test
    public void testBuilder() {
        final LocalDateTime dhAppointment = LocalDateTime.now();
        final String description = "description";

        final AppointmentDto dto = AppointmentDto.Builder.create()
                .pet(PetDto.Builder.create().build())
                .employee(EmployeeDto.Builder.create().build())
                .dhAppointment(dhAppointment)
                .description(description)
                .build();

        assertEquals(dhAppointment, dto.getDhAppointment());
        assertEquals(description, dto.getDescription());

        assertNotNull(dto.getPet());
        assertNotNull(dto.getEmployee());

        assertNull(dto.getDhScheduling());
    }
}