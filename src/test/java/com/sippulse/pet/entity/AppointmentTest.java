package com.sippulse.pet.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;

import org.junit.Test;

public class AppointmentTest {

    @Test
    public void testBuilder() {
        final LocalDateTime dhAppointment = LocalDateTime.now();
        final String description = "description";

        final Appointment appointment = Appointment.Builder.create()
                .pet(Pet.Builder.create().build())
                .employee(Employee.Builder.create().build())
                .dhAppointment(dhAppointment)
                .description(description)
                .build();

        assertEquals(dhAppointment, appointment.getDhAppointment());
        assertEquals(description, appointment.getDescription());

        assertNotNull(appointment.getPet());
        assertNotNull(appointment.getEmployee());
        assertNotNull(appointment.getDhScheduling());
    }
}