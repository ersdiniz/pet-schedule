package com.sippulse.pet.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.sippulse.pet.enums.TypeEmployee;

public class EmployeeTest {

    @Test
    public void testBuilder() {
        final String name = "Rex";
        final String cpf = "12312312312";
        final TypeEmployee type = TypeEmployee.VET;
        final String username = "username";
        final String password = "password";

        final Employee employee = Employee.Builder.create()
                .name(name)
                .cpf(cpf)
                .type(type)
                .username(username)
                .password(password)
                .build();

        assertEquals(name, employee.getName());
        assertEquals(cpf, employee.getCpf());
        assertEquals(type, employee.getType());
        assertEquals(username, employee.getUsername());
        assertEquals(password, employee.getPassword());
    }
}