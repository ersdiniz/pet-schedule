package com.sippulse.pet.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClientTest {

    @Test
    public void testBuilder() {
        final String name = "Jon";
        final String cpf = "12312312312";

        final Client client = Client.Builder.create()
                .name(name)
                .cpf(cpf)
                .build();

        assertEquals(name, client.getName());
        assertEquals(cpf, client.getCpf());
    }
}