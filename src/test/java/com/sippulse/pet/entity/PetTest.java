package com.sippulse.pet.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PetTest {

    @Test
    public void testBuilder() {
        final String name = "Rex";
        final String description = "Dog";

        final Pet pet = Pet.Builder.create()
                .name(name)
                .description(description)
                .client(Client.Builder.create().build())
                .build();

        assertEquals(name, pet.getName());
        assertEquals(description, pet.getDescription());
    }
}