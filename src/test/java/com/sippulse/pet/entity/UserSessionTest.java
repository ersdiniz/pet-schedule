package com.sippulse.pet.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserSessionTest {

    @Test
    public void testBuilder() {
        final String token = "token";

        final UserSession userSession = UserSession.Builder.create()
                .token(token)
                .build();

        assertEquals(token, userSession.getToken());
    }
}