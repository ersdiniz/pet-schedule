package com.sippulse.pet.controller.v1;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sippulse.pet.dto.AppointmentDto;
import com.sippulse.pet.dto.AppointmentJdbcDto;
import com.sippulse.pet.dto.AppointmentSimplifiedDto;
import com.sippulse.pet.entity.Appointment;
import com.sippulse.pet.exception.PetScheduleEntityNotFoundException;
import com.sippulse.pet.service.AppointmentService;
import com.sippulse.pet.utils.ResponseEntityUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/api/v1/appointments", produces = MediaType.APPLICATION_JSON_VALUE)
public class AppointmentController {

    private static final String NOT_FOUND = "Appointment not found";

    @Autowired
    private AppointmentService service;
    @Autowired
    private AppointmentDto.RepresentationBuilder builder;

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No appointments found"),
    })
    @ApiOperation(value = "findAll", notes = "Find appointments by vet identifier, part name of vet or date appointment")
    @GetMapping
    public ResponseEntity<List<AppointmentDto>> findAll(
            @RequestParam(value = "vetId", required = false) final Long vetId,
            @RequestParam(value = "vetName", required = false) final String vetName,
            @RequestParam(value = "date", required = false) final String date) {
        final List<AppointmentJdbcDto> appointments = service.findAll(vetId, vetName, date);
        return ResponseEntityUtil.okOrNoContent(builder.toJdbcRepresentation(appointments));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No appointments found"),
    })
    @ApiOperation(value = "findByCpf", notes = "Find appointments by client cpf. Don't require authorization header.")
    @GetMapping(path = "/cpf/{cpf}")
    public ResponseEntity<List<AppointmentDto>> findByCpf(@PathVariable(name = "cpf") final String cpf) {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findAllByClientCpf(cpf)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = NOT_FOUND),
    })
    @ApiOperation(value = "find", notes = "Find a appointment by id")
    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<AppointmentDto> find(@PathVariable(name = "id") final Long id) {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findById(id)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Appointment created"),
    })
    @ApiOperation(value = "persist", notes = "Create a appointment")
    @PostMapping
    public ResponseEntity<AppointmentDto> persist(@Valid @RequestBody final AppointmentSimplifiedDto representation) {
        final Appointment persisted = service.persist(builder.fromRepresentation(representation));
        return ResponseEntityUtil.created(builder.toRepresentation(persisted));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "merge", notes = "Update a appointment by appointment identifier")
    @PutMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<AppointmentDto> merge(
            @PathVariable(name = "id") final Long id,
            @Valid @RequestBody final AppointmentDto representation) {
        final Appointment existing = getEntityOrElseThrow(id);
        final Appointment merged = builder.fromRepresentation(representation, Appointment.Builder.from(existing));
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.persist(merged)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "delete", notes = "Delete a appointment by appointment identifier")
    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<AppointmentDto> delete(@PathVariable(name = "id") final Long id) {
        final Appointment existing = getEntityOrElseThrow(id);
        service.delete(existing.getId());
        return ResponseEntity.ok().build();
    }

    private Appointment getEntityOrElseThrow(final Long id) {
        return Optional.ofNullable(service.findById(id)).orElseThrow(() -> new PetScheduleEntityNotFoundException(NOT_FOUND));
    }
}