package com.sippulse.pet.controller.v1;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sippulse.pet.dto.EmployeeDto;
import com.sippulse.pet.dto.EmployeeSimplifiedDto;
import com.sippulse.pet.entity.Employee;
import com.sippulse.pet.exception.PetScheduleEntityNotFoundException;
import com.sippulse.pet.service.EmployeeService;
import com.sippulse.pet.utils.ResponseEntityUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/api/v1/employees", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeController {

    private static final String NOT_FOUND = "Employee not found";

    @Autowired
    private EmployeeService service;
    @Autowired
    private EmployeeDto.RepresentationBuilder builder;

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No employees found"),
    })
    @ApiOperation(value = "findAll", notes = "Find all employees")
    @GetMapping
    public ResponseEntity<List<EmployeeDto>> findAll() {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findAll()));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = NOT_FOUND),
    })
    @ApiOperation(value = "find", notes = "Find a employee by id")
    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<EmployeeDto> find(@PathVariable(name = "id") final Long id) {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findById(id)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Employee created"),
    })
    @ApiOperation(value = "persist", notes = "Create a employee")
    @PostMapping
    public ResponseEntity<EmployeeDto> persist(@Valid @RequestBody final EmployeeSimplifiedDto representation) {
        final Employee persisted = service.persist(builder.fromRepresentation(representation));
        return ResponseEntityUtil.created(builder.toRepresentation(persisted));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "merge", notes = "Update a employee by employee identifier")
    @PutMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<EmployeeDto> merge(
            @PathVariable(name = "id") final Long id,
            @Valid @RequestBody final EmployeeDto representation) {
        final Employee existing = getEntityOrElseThrow(id);
        final Employee merged = builder.fromRepresentation(representation, Employee.Builder.from(existing));
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.persist(merged)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "delete", notes = "Delete a employee by employee identifier")
    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<EmployeeDto> delete(@PathVariable(name = "id") final Long id) {
        final Employee existing = getEntityOrElseThrow(id);
        service.delete(existing.getId());
        return ResponseEntity.ok().build();
    }

    private Employee getEntityOrElseThrow(final Long id) {
        return Optional.ofNullable(service.findById(id)).orElseThrow(() -> new PetScheduleEntityNotFoundException(NOT_FOUND));
    }
}