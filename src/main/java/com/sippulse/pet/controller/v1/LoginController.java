package com.sippulse.pet.controller.v1;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sippulse.pet.dto.login.LoginRequestDto;
import com.sippulse.pet.dto.login.LoginResponseDto;
import com.sippulse.pet.entity.UserSession;
import com.sippulse.pet.exception.PetScheduleLoginException;
import com.sippulse.pet.security.Encryptor;
import com.sippulse.pet.service.EmployeeService;
import com.sippulse.pet.service.UserSessionService;
import com.sippulse.pet.utils.ResponseEntityUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class LoginController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private UserSessionService loginService;

    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Not authorized"),
    })
    @ApiOperation(value = "signin", notes = "Create a valid token to user. Don't require authorization header.")
    @PostMapping(path = "/signin")
    public ResponseEntity<LoginResponseDto> signin(@Valid @RequestBody final LoginRequestDto representation) {
        if (!employeeService.existsByUsernameAndPassword(representation.getUsername(), representation.getPassword())) {
            throw new PetScheduleLoginException("Invalid username or password");
        }

        final String token = Encryptor.encrypt(representation.getUsername());

        loginService.persist(UserSession.Builder.create().token(token).build());

        return ResponseEntityUtil.okOrNoContent(LoginResponseDto.of(token));
    }
}