package com.sippulse.pet.controller.v1;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sippulse.pet.dto.PetDto;
import com.sippulse.pet.dto.PetSimplifiedDto;
import com.sippulse.pet.entity.Pet;
import com.sippulse.pet.exception.PetScheduleEntityNotFoundException;
import com.sippulse.pet.service.PetService;
import com.sippulse.pet.utils.ResponseEntityUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/api/v1/pets", produces = MediaType.APPLICATION_JSON_VALUE)
public class PetController {

    private static final String NOT_FOUND = "Pet not found";

    @Autowired
    private PetService service;
    @Autowired
    private PetDto.RepresentationBuilder builder;

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No pets found"),
    })
    @ApiOperation(value = "findAll", notes = "Find all pets")
    @GetMapping
    public ResponseEntity<List<PetDto>> findAll() {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findAll()));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = NOT_FOUND),
    })
    @ApiOperation(value = "find", notes = "Find a pet by id")
    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<PetDto> find(@PathVariable(name = "id") final Long id) {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findById(id)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Pet created"),
    })
    @ApiOperation(value = "persist", notes = "Create a pet")
    @PostMapping
    public ResponseEntity<PetDto> persist(@Valid @RequestBody final PetSimplifiedDto representation) {
        final Pet persisted = service.persist(builder.fromRepresentation(representation));
        return ResponseEntityUtil.created(builder.toRepresentation(persisted));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "merge", notes = "Update a pet by pet identifier")
    @PutMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<PetDto> merge(
            @PathVariable(name = "id") final Long id,
            @Valid @RequestBody final PetDto representation) {
        final Pet existing = getEntityOrElseThrow(id);
        final Pet merged = builder.fromRepresentation(representation, Pet.Builder.from(existing));
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.persist(merged)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "delete", notes = "Delete a pet by pet identifier")
    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<PetDto> delete(@PathVariable(name = "id") final Long id) {
        final Pet existing = getEntityOrElseThrow(id);
        service.delete(existing.getId());
        return ResponseEntity.ok().build();
    }

    private Pet getEntityOrElseThrow(final Long id) {
        return Optional.ofNullable(service.findById(id)).orElseThrow(() -> new PetScheduleEntityNotFoundException(NOT_FOUND));
    }
}