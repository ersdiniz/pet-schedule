package com.sippulse.pet.controller.v1;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sippulse.pet.dto.ClientDto;
import com.sippulse.pet.dto.ClientSimplifiedDto;
import com.sippulse.pet.entity.Client;
import com.sippulse.pet.exception.PetScheduleEntityNotFoundException;
import com.sippulse.pet.service.ClientService;
import com.sippulse.pet.utils.ResponseEntityUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/api/v1/clients", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientController {

    private static final String NOT_FOUND = "Client not found";

    @Autowired
    private ClientService service;
    @Autowired
    private ClientDto.RepresentationBuilder builder;

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No clients found"),
    })
    @ApiOperation(value = "findAll", notes = "Find all clients")
    @GetMapping
    public ResponseEntity<List<ClientDto>> findAll() {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findAll()));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = NOT_FOUND),
    })
    @ApiOperation(value = "find", notes = "Find a client by id")
    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<ClientDto> find(@PathVariable(name = "id") final Long id) {
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.findById(id)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Client created"),
    })
    @ApiOperation(value = "persist", notes = "Create a client")
    @PostMapping
    public ResponseEntity<ClientDto> persist(@Valid @RequestBody final ClientSimplifiedDto representation) {
        final Client persisted = service.persist(builder.fromRepresentation(representation));
        return ResponseEntityUtil.created(builder.toRepresentation(persisted));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "merge", notes = "Update a client by client identifier")
    @PutMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<ClientDto> merge(
            @PathVariable(name = "id") final Long id,
            @Valid @RequestBody final ClientDto representation) {
        final Client existing = getEntityOrElseThrow(id);
        final Client merged = builder.fromRepresentation(representation, Client.Builder.from(existing));
        return ResponseEntityUtil.okOrNoContent(builder.toRepresentation(service.persist(merged)));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 422, message = NOT_FOUND),
    })
    @ApiOperation(value = "delete", notes = "Delete a client by client identifier")
    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<ClientDto> delete(@PathVariable(name = "id") final Long id) {
        final Client existing = getEntityOrElseThrow(id);
        service.delete(existing.getId());
        return ResponseEntity.ok().build();
    }

    private Client getEntityOrElseThrow(final Long id) {
        return Optional.ofNullable(service.findById(id)).orElseThrow(() -> new PetScheduleEntityNotFoundException(NOT_FOUND));
    }
}