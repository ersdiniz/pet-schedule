package com.sippulse.pet.service;

import java.util.List;

import com.sippulse.pet.dto.AppointmentJdbcDto;
import com.sippulse.pet.entity.Appointment;

public interface AppointmentService {

    List<Appointment> findAll();

    List<AppointmentJdbcDto> findAll(final Long vetId, final String vetName, final String date);

    List<Appointment> findAllByPetId(final Long petId);

    List<Appointment> findAllByEmployeeId(final Long employeeId);

    List<Appointment> findAllByClientCpf(final String cpf);

    Appointment findById(final Long id);

    Appointment persist(final Appointment entity);

    void delete(final Long id);
}