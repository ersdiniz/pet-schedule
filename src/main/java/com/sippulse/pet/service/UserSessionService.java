package com.sippulse.pet.service;

import com.sippulse.pet.entity.UserSession;

public interface UserSessionService {

    boolean isValid(final String token);

    UserSession persist(final UserSession entity);
}