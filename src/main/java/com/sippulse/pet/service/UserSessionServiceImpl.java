package com.sippulse.pet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sippulse.pet.entity.UserSession;
import com.sippulse.pet.repository.UserSessionRepository;

@Service
public class UserSessionServiceImpl implements UserSessionService {

    @Autowired
    private UserSessionRepository repository;

    @Override
    public boolean isValid(final String token) {
        return repository.existsByToken(token);
    }

    @Override
    public UserSession persist(final UserSession entity) {
        return repository.save(entity);
    }
}