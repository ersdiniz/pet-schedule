package com.sippulse.pet.service;

import java.util.List;

import com.sippulse.pet.entity.Employee;
import com.sippulse.pet.enums.TypeEmployee;

public interface EmployeeService {

    List<Employee> findAll();

    List<Employee> findAllByType(final TypeEmployee type);

    List<Employee> findByName(final String name);

    Employee findByCpf(final String cpf);

    Employee findById(final Long id);

    Employee persist(final Employee entity);

    void delete(final Long id);

    boolean existsByUsernameAndPassword(final String username, final String password);
}