package com.sippulse.pet.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sippulse.pet.dto.AppointmentJdbcDto;
import com.sippulse.pet.entity.Appointment;
import com.sippulse.pet.repository.AppointmentJdbcRepository;
import com.sippulse.pet.repository.AppointmentRepository;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentRepository repository;

    @Autowired
    private AppointmentJdbcRepository jdbcRepository;

    @Override
    public Appointment findById(final Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Appointment> findAll() {
        return repository.findByOrderByDhAppointmentDesc();
    }

    @Override
    public List<AppointmentJdbcDto> findAll(final Long vetId, final String vetName, final String date) {
        return jdbcRepository.findAll(vetId, vetName, date);
    }

    @Override
    public List<Appointment> findAllByPetId(final Long petId) {
        return repository.findByPetId(petId);
    }

    @Override
    public List<Appointment> findAllByEmployeeId(final Long employeeId) {
        return repository.findByEmployeeId(employeeId);
    }

    @Override
    public Appointment persist(final Appointment entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(final Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Appointment> findAllByClientCpf(final String cpf) {
        if (cpf == null || cpf.isEmpty()) {
            return Collections.emptyList();
        }
        return repository.findByPetClientCpf(cpf.replace(".", "").replace("-", ""));
    }
}