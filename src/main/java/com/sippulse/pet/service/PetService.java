package com.sippulse.pet.service;

import java.util.List;

import com.sippulse.pet.entity.Pet;

public interface PetService {

    List<Pet> findAll();

    List<Pet> findByName(final String name);

    List<Pet> findByClient(final Long clientId);

    Pet findById(final Long id);

    Pet persist(final Pet entity);

    void delete(final Long id);
}