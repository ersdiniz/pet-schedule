package com.sippulse.pet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sippulse.pet.entity.Pet;
import com.sippulse.pet.repository.PetRepository;

@Service
public class PetServiceImpl implements PetService {

    @Autowired
    private PetRepository repository;

    @Override
    public List<Pet> findAll() {
        return repository.findByOrderByNameAsc();
    }

    @Override
    public List<Pet> findByName(final String name) {
        return repository.findByNameIgnoreCaseContainingOrderByNameAsc(name);
    }

    @Override
    public List<Pet> findByClient(final Long clientId) {
        return repository.findByClientId(clientId);
    }

    @Override
    public Pet findById(final Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Pet persist(final Pet entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(final Long id) {
        repository.deleteById(id);
    }
}