package com.sippulse.pet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sippulse.pet.entity.Employee;
import com.sippulse.pet.enums.TypeEmployee;
import com.sippulse.pet.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> findAll() {
        return repository.findByOrderByNameAsc();
    }

    @Override
    public List<Employee> findAllByType(final TypeEmployee type) {
        return repository.findByTypeOrderByNameAsc(type);
    }

    @Override
    public List<Employee> findByName(final String name) {
        return repository.findByNameIgnoreCaseContainingOrderByNameAsc(name);
    }

    @Override
    public Employee findByCpf(final String cpf) {
        return repository.findByCpf(cpf);
    }

    @Override
    public Employee findById(final Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Employee persist(final Employee entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(final Long id) {
        repository.deleteById(id);
    }

    @Override
    public boolean existsByUsernameAndPassword(final String username, final String password) {
        return repository.existsByUsernameAndPassword(username, password);
    }
}