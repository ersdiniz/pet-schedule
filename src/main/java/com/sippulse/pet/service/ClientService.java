package com.sippulse.pet.service;

import java.util.List;

import com.sippulse.pet.entity.Client;

public interface ClientService {

    List<Client> findAll();

    List<Client> findByName(final String name);

    Client findById(final Long id);

    Client persist(final Client entity);

    void delete(final Long id);
}