package com.sippulse.pet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sippulse.pet.entity.Client;
import com.sippulse.pet.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository repository;

    @Override
    public Client findById(final Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Client> findAll() {
        return repository.findByOrderByNameAsc();
    }

    @Override
    public List<Client> findByName(final String name) {
        return repository.findByNameIgnoreCaseContainingOrderByNameAsc(name);
    }

    @Override
    public Client persist(final Client entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(final Long id) {
        repository.deleteById(id);
    }
}