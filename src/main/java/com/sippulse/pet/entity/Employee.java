package com.sippulse.pet.entity;

import static javax.persistence.EnumType.STRING;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sippulse.pet.enums.TypeEmployee;
import com.sippulse.pet.utils.AbstractBuilder;

/**
 * Entidade de empregados, sejam veterinários ou não
 */
@SuppressWarnings("serial")
@Entity
public class Employee extends AbstractPeople {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(STRING)
    private TypeEmployee type = TypeEmployee.EMPLOYEE;

    private String username;
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeEmployee getType() {
        return type;
    }

    public void setType(TypeEmployee type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Employee)) {
            return false;
        }
        return this.getId() == ((Employee) obj).getId();
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (id != null) {
            result = 31 * result + id.hashCode();
        }
        return result;
    }

    public static class Builder extends AbstractBuilder<Employee> {

        public Builder(final Employee entity) {
            super(entity);
        }

        public static Builder create() {
            return new Builder(new Employee());
        }

        public static Builder from(final Employee entity) {
            return new Builder(entity);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder name(final String name) {
            item.setName(name);
            return this;
        }

        public Builder cpf(final String cpf) {
            item.setCpf(cpf);
            return this;
        }

        public Builder type(final TypeEmployee type) {
            item.setType(type);
            return this;
        }

        public Builder username(final String username) {
            item.setUsername(username);
            return this;
        }

        public Builder password(final String password) {
            item.setPassword(password);
            return this;
        }
    }
}