package com.sippulse.pet.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.sippulse.pet.utils.AbstractBuilder;

/**
 * Entidade de clientes
 */
@SuppressWarnings("serial")
@Entity
public class Client extends AbstractPeople {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "client")
    private List<Pet> pets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(final List<Pet> pets) {
        this.pets = pets;
    }

    public void addPet(final Pet pet) {
        this.pets.add(pet);
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Employee)) {
            return false;
        }
        return this.getId() == ((Employee) obj).getId();
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (id != null) {
            result = 31 * result + id.hashCode();
        }
        return result;
    }

    public static class Builder extends AbstractBuilder<Client> {

        public Builder(final Client entity) {
            super(entity);
        }

        public static Builder create() {
            return new Builder(new Client());
        }

        public static Builder from(final Client entity) {
            return new Builder(entity);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder name(final String name) {
            item.setName(name);
            return this;
        }

        public Builder cpf(final String cpf) {
            item.setCpf(cpf);
            return this;
        }

        public Builder pets(final List<Pet> pets) {
            item.setPets(pets);
            return this;
        }

        public Builder addPet(final Pet pet) {
            item.addPet(pet);
            return this;
        }
    }
}