package com.sippulse.pet.entity;

import static javax.persistence.FetchType.LAZY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.IEntity;

/**
 * Entidade de animais de estimação
 */
@SuppressWarnings("serial")
@Entity
public class Pet implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;

    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "i_clients")
    private Client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Pet)) {
            return false;
        }
        return this.getId() == ((Pet) obj).getId();
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (id != null) {
            result = 31 * result + id.hashCode();
        }
        return result;
    }

    public static class Builder extends AbstractBuilder<Pet> {

        public Builder(final Pet entity) {
            super(entity);
        }

        public static Builder create() {
            return new Builder(new Pet());
        }

        public static Builder from(final Pet entity) {
            return new Builder(entity);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder client(final Client client) {
            item.setClient(client);
            return this;
        }

        public Builder name(final String name) {
            item.setName(name);
            return this;
        }

        public Builder description(final String description) {
            item.setDescription(description);
            return this;
        }
    }
}