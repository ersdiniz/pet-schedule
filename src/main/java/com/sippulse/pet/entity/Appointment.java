package com.sippulse.pet.entity;

import static javax.persistence.FetchType.LAZY;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.IEntity;

/**
 * Entidade de consultas agendadas
 */
@SuppressWarnings("serial")
@Entity
public class Appointment implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "i_pets")
    private Pet pet;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "i_employees")
    private Employee employee;

    @Column(name = "dh_scheduling")
    private LocalDateTime dhScheduling;

    @Column(name = "dh_appointment")
    private LocalDateTime dhAppointment;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(final Pet pet) {
        this.pet = pet;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(final Employee employee) {
        this.employee = employee;
    }

    public LocalDateTime getDhScheduling() {
        return dhScheduling;
    }

    public void setDhScheduling(final LocalDateTime dhScheduling) {
        this.dhScheduling = dhScheduling;
    }

    public LocalDateTime getDhAppointment() {
        return dhAppointment;
    }

    public void setDhAppointment(final LocalDateTime dhAppointment) {
        this.dhAppointment = dhAppointment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Appointment)) {
            return false;
        }
        return this.getId() == ((Appointment) obj).getId();
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (id != null) {
            result = 31 * result + id.hashCode();
        }
        return result;
    }

    public static class Builder extends AbstractBuilder<Appointment> {

        public Builder(final Appointment entity) {
            super(entity);
        }

        public static Builder create() {
            return new Builder(new Appointment());
        }

        public static Builder from(final Appointment entity) {
            return new Builder(entity);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder pet(final Pet pet) {
            item.setPet(pet);
            return this;
        }

        public Builder employee(final Employee employee) {
            item.setEmployee(employee);
            return this;
        }

        public Builder dhAppointment(final LocalDateTime dhAppointment) {
            item.setDhAppointment(dhAppointment);
            return this;
        }

        public Builder description(final String description) {
            item.setDescription(description);
            return this;
        }

        @Override
        public Appointment build() {
            item.setDhScheduling(LocalDateTime.now());
            return super.build();
        }
    }
}