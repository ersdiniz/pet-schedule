package com.sippulse.pet.entity;

import javax.persistence.MappedSuperclass;

import com.sippulse.pet.utils.IEntity;

@SuppressWarnings("serial")
@MappedSuperclass
public class AbstractPeople implements IEntity {

    private String name;
    private String cpf;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}