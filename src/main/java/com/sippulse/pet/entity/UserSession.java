package com.sippulse.pet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.IEntity;

/**
 * Entidade da sessão do usuário logado
 */
@SuppressWarnings("serial")
@Entity
public class UserSession implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof UserSession)) {
            return false;
        }
        return this.getId() == ((UserSession) obj).getId();
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (id != null) {
            result = 31 * result + id.hashCode();
        }
        return result;
    }

    public static class Builder extends AbstractBuilder<UserSession> {

        public Builder(final UserSession entity) {
            super(entity);
        }

        public static Builder create() {
            return new Builder(new UserSession());
        }

        public static Builder from(final UserSession entity) {
            return new Builder(entity);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder token(final String token) {
            item.setToken(token);
            return this;
        }
    }
}