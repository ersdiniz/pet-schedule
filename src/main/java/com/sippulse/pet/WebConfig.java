package com.sippulse.pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.sippulse.pet.security.InterceptorConfig;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private InterceptorConfig interceptorConfig;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptorConfig)
                .addPathPatterns("/api/**")
                .excludePathPatterns("/api/v1/appointments/cpf/**");
    }
}