package com.sippulse.pet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sippulse.pet.entity.Employee;
import com.sippulse.pet.enums.TypeEmployee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByCpf(final String cpf);

    List<Employee> findByOrderByNameAsc();

    List<Employee> findByTypeOrderByNameAsc(final TypeEmployee type);

    List<Employee> findByNameIgnoreCaseContainingOrderByNameAsc(final String name);

    boolean existsByUsernameAndPassword(final String username, final String password);
}