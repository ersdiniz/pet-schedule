package com.sippulse.pet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sippulse.pet.entity.Appointment;

@Repository
public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

    List<Appointment> findByOrderByDhAppointmentDesc();

    List<Appointment> findByPetId(final Long petId);

    List<Appointment> findByEmployeeId(final Long employeeId);

    List<Appointment> findByPetClientCpf(final String cpf);
}