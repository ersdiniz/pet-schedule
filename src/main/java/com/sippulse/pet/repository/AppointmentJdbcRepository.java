package com.sippulse.pet.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sippulse.pet.dto.AppointmentJdbcDto;

@Repository
public class AppointmentJdbcRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Busca as consultas agendadas, podendo ser filtrado pelo identificador do
     * veterinário, por parte do seu nome e/ou pela data do agendamento
     * 
     * @param vetId
     * @param vetName
     * @param date
     * @return lista de consultas agendadas
     */
    public List<AppointmentJdbcDto> findAll(final Long vetId, final String vetName, final String date) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT appointment.id,");
        sql.append("       appointment.description,");
        sql.append("       pet.id AS petId,");
        sql.append("       employee.id AS employeeId,");
        sql.append("       appointment.dh_scheduling AS dhScheduling,");
        sql.append("       appointment.dh_appointment AS dhAppointment");
        sql.append("  FROM appointment");
        sql.append("  JOIN employee ON (employee.id = appointment.i_employees)");
        sql.append("  JOIN pet ON (pet.id = appointment.i_pets)");
        sql.append("  JOIN client ON (client.id = pet.i_clients) ");

        final List<Object> params = new ArrayList<>();
        if (vetId != null) {
            sql.append(" WHERE appointment.i_employees = ? ");
            params.add(vetId);
        }
        if (vetName != null && !vetName.isEmpty()) {
            sql.append(params.isEmpty() ? "WHERE" : "AND").append(" upper(employee.name) LIKE ? ");
            params.add("%".concat(vetName.toUpperCase()).concat("%"));
        }
        if (date != null) {
            sql.append(params.isEmpty() ? "WHERE" : "AND").append(" DATE(appointment.dh_appointment) = ? ");
            params.add(date);
        }
        return jdbcTemplate.query(sql.toString(), params.stream().toArray(), BeanPropertyRowMapper.newInstance(AppointmentJdbcDto.class));
    }
}