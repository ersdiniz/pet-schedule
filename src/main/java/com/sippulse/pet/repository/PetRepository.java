package com.sippulse.pet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sippulse.pet.entity.Pet;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long> {

    List<Pet> findByOrderByNameAsc();

    List<Pet> findByNameIgnoreCaseContainingOrderByNameAsc(final String name);

    List<Pet> findByClientId(final Long clientId);
}