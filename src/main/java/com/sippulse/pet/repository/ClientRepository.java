package com.sippulse.pet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sippulse.pet.entity.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    List<Client> findByOrderByNameAsc();

    List<Client> findByNameIgnoreCaseContainingOrderByNameAsc(final String name);
}