package com.sippulse.pet.repository;

import org.springframework.data.repository.CrudRepository;

import com.sippulse.pet.entity.UserSession;

public interface UserSessionRepository extends CrudRepository<UserSession, Long> {

    boolean existsByToken(final String token);
}