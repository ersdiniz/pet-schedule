package com.sippulse.pet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class PetScheduleEntityNotFoundException extends RuntimeException {

    public PetScheduleEntityNotFoundException() {
        super();
    }

    public PetScheduleEntityNotFoundException(final String message) {
        super(message);
    }

    public PetScheduleEntityNotFoundException(final String message, final Exception e) {
        super(message, e);
    }
}