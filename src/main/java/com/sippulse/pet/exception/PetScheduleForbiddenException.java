package com.sippulse.pet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class PetScheduleForbiddenException extends RuntimeException {

    public PetScheduleForbiddenException() {
        super();
    }

    public PetScheduleForbiddenException(final String message) {
        super(message);
    }

    public PetScheduleForbiddenException(final String message, final Exception e) {
        super(message, e);
    }
}