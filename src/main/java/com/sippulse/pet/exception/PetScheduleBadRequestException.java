package com.sippulse.pet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PetScheduleBadRequestException extends RuntimeException {

    public PetScheduleBadRequestException() {
        super();
    }

    public PetScheduleBadRequestException(final String message) {
        super(message);
    }

    public PetScheduleBadRequestException(final String message, final Exception e) {
        super(message, e);
    }
}