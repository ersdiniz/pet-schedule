package com.sippulse.pet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class PetScheduleLoginException extends RuntimeException {

    public PetScheduleLoginException() {
        super();
    }

    public PetScheduleLoginException(final String message) {
        super(message);
    }

    public PetScheduleLoginException(final String message, final Exception e) {
        super(message, e);
    }
}