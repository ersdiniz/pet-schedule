package com.sippulse.pet.utils;

import java.lang.reflect.Array;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntityUtil {

    private ResponseEntityUtil() {
    }

    /**
     * Resposta padrão para APIs que criam entidades
     * 
     * @param <T> body
     * @return ResponseEntity com body e status 201
     */
    public static <T> ResponseEntity<T> created(final T body) {
        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }

    /**
     * Reposta padrão para APIs. Se o objeto passado por parâmetro não estiver nulo
     * ou for uma lista vazia, o status passa a ser 204.
     * 
     * @param <T> body
     * @return ResponseEntity
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseEntity<T> okOrNoContent(final T body) {
        if (body != null) {
            if (body instanceof List) {
                return !((List<T>) body).isEmpty() ? ResponseEntity.ok(body) : ResponseEntity.noContent().build();
            }
            if (body.getClass().isArray()) {
                return Array.getLength(body) > 0 ? ResponseEntity.ok(body) : ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(body);
        }
        return ResponseEntity.noContent().build();
    }
}