package com.sippulse.pet.utils;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepresentationBuilder<E extends IEntity, R extends IDto, B extends AbstractBuilder<E>> {

    public abstract E fromRepresentation(final R representation, final B builder);

    public abstract R toRepresentation(final E entity);

    public List<R> toRepresentation(final List<E> list) {
        return list.stream().map(this::toRepresentation).collect(Collectors.toList());
    }
}