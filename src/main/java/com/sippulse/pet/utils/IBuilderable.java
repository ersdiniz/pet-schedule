package com.sippulse.pet.utils;

import java.io.Serializable;

/**
 * Interface padrão para classes que utilizam um builder para sua manipulação
 */
public interface IBuilderable extends Serializable {
}