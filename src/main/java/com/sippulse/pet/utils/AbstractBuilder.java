package com.sippulse.pet.utils;

public abstract class AbstractBuilder<B extends IBuilderable> {

    protected B item;

    public AbstractBuilder(final B item) {
        this.item = item;
    }

    public B build() {
        return item;
    }
}