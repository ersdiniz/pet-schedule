package com.sippulse.pet.utils;

/**
 * Interface padrão para representações de entidades
 */
public interface IDto extends IBuilderable {
}