package com.sippulse.pet.dto.login;

import com.sippulse.pet.utils.IDto;

@SuppressWarnings("serial")
public class LoginResponseDto implements IDto {

    private String token;

    public LoginResponseDto() {
    }

    public LoginResponseDto(final String token) {
        setToken(token);
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public static LoginResponseDto of(final String token) {
        return new LoginResponseDto(token);
    }
}