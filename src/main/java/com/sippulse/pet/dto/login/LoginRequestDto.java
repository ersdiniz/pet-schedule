package com.sippulse.pet.dto.login;

import com.sippulse.pet.utils.IDto;

@SuppressWarnings("serial")
public class LoginRequestDto implements IDto {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}