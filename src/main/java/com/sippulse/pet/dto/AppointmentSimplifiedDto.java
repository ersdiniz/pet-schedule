package com.sippulse.pet.dto;

import java.time.LocalDateTime;

import com.sippulse.pet.entity.Appointment;
import com.sippulse.pet.utils.IDto;

/**
 * Representação simplificada da entidade {@link Appointment} para cadastro
 */
@SuppressWarnings("serial")
public class AppointmentSimplifiedDto implements IDto {

    private Long petId;
    private Long employeeId;
    private LocalDateTime dhAppointment;
    private String description;

    public Long getPetId() {
        return petId;
    }

    public void setPetId(final Long petId) {
        this.petId = petId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(final Long employeeId) {
        this.employeeId = employeeId;
    }

    public LocalDateTime getDhAppointment() {
        return dhAppointment;
    }

    public void setDhAppointment(final LocalDateTime dhAppointment) {
        this.dhAppointment = dhAppointment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}