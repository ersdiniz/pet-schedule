package com.sippulse.pet.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sippulse.pet.entity.Client;
import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.AbstractRepresentationBuilder;
import com.sippulse.pet.utils.IDto;

/**
 * Representação da entidade {@link Client} para retorno da API
 */
@SuppressWarnings("serial")
public class ClientDto implements IDto {

    private Long id;
    private String name;
    private String cpf;
    private List<PetDto> pets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<PetDto> getPets() {
        return pets;
    }

    public void setPets(final List<PetDto> pets) {
        this.pets = pets;
    }

    public void addPet(final PetDto pet) {
        this.pets.add(pet);
    }

    @Component
    public static final class RepresentationBuilder extends AbstractRepresentationBuilder<Client, ClientDto, Client.Builder> {

        @Autowired
        private PetDto.RepresentationBuilder petDtoBuilder;

        public Client fromRepresentation(final ClientSimplifiedDto representation) {
            if (representation == null) {
                return null;
            }
            return Client.Builder.create()
                    .name(representation.getName())
                    .cpf(representation.getCpf())
                    .build();
        }

        @Override
        public Client fromRepresentation(final ClientDto representation, final Client.Builder builder) {
            if (representation == null) {
                return null;
            }
            return builder
                    .id(representation.getId())
                    .name(representation.getName())
                    .cpf(representation.getCpf())
                    .build();
        }

        @Override
        public ClientDto toRepresentation(final Client entity) {
            if (entity == null) {
                return null;
            }
            return ClientDto.Builder.create()
                    .id(entity.getId())
                    .name(entity.getName())
                    .cpf(entity.getCpf())
                    .pets(petDtoBuilder.toRepresentation(entity.getPets()))
                    .build();
        }
    }

    public static class Builder extends AbstractBuilder<ClientDto> {

        public Builder(final ClientDto dto) {
            super(dto);
        }

        public static Builder create() {
            return new Builder(new ClientDto());
        }

        public static Builder from(final ClientDto dto) {
            return new Builder(dto);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder name(final String name) {
            item.setName(name);
            return this;
        }

        public Builder cpf(final String cpf) {
            item.setCpf(cpf);
            return this;
        }

        public Builder pets(final List<PetDto> pets) {
            item.setPets(pets);
            return this;
        }

        public Builder addPet(final PetDto pet) {
            item.addPet(pet);
            return this;
        }
    }
}