package com.sippulse.pet.dto;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sippulse.pet.entity.Pet;
import com.sippulse.pet.service.ClientService;
import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.AbstractRepresentationBuilder;
import com.sippulse.pet.utils.IDto;

/**
 * Representação da entidade {@link Pet} para retorno da API
 */
@SuppressWarnings("serial")
public class PetDto implements IDto {

    private Long id;
    private String name;
    private String description;
    private Long clientId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    @Component
    public static final class RepresentationBuilder extends AbstractRepresentationBuilder<Pet, PetDto, Pet.Builder> {

        @Autowired
        private ClientService clientService;

        public Pet fromRepresentation(final PetSimplifiedDto representation) {
            if (representation == null) {
                return null;
            }
            return Pet.Builder.create()
                    .client(Optional.ofNullable(representation.getClientId())
                            .map(clientService::findById)
                            .orElse(null))
                    .name(representation.getName())
                    .description(representation.getDescription())
                    .build();
        }

        @Override
        public Pet fromRepresentation(final PetDto representation, final Pet.Builder builder) {
            if (representation == null) {
                return null;
            }
            return builder
                    .id(representation.getId())
                    .client(Optional.ofNullable(representation.getClientId())
                            .map(clientService::findById)
                            .orElse(null))
                    .name(representation.getName())
                    .description(representation.getDescription())
                    .build();
        }

        @Override
        public PetDto toRepresentation(final Pet entity) {
            if (entity == null) {
                return null;
            }
            return PetDto.Builder.create()
                    .id(entity.getId())
                    .clientId(entity.getClient().getId())
                    .name(entity.getName())
                    .description(entity.getDescription())
                    .build();
        }
    }

    public static class Builder extends AbstractBuilder<PetDto> {

        public Builder(final PetDto dto) {
            super(dto);
        }

        public static Builder create() {
            return new Builder(new PetDto());
        }

        public static Builder from(final PetDto dto) {
            return new Builder(dto);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder name(final String name) {
            item.setName(name);
            return this;
        }

        public Builder description(final String description) {
            item.setDescription(description);
            return this;
        }

        public Builder clientId(final Long clientId) {
            item.setClientId(clientId);
            return this;
        }
    }
}