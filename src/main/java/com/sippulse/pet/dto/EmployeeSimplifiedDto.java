package com.sippulse.pet.dto;

import com.sippulse.pet.entity.Employee;
import com.sippulse.pet.enums.TypeEmployee;
import com.sippulse.pet.utils.IDto;

/**
 * Representação simplificada da entidade {@link Employee} para cadastro
 */
@SuppressWarnings("serial")
public class EmployeeSimplifiedDto implements IDto {

    private String name;
    private String cpf;
    private String username;
    private String password;
    private TypeEmployee type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public TypeEmployee getType() {
        return type;
    }

    public void setType(TypeEmployee type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}