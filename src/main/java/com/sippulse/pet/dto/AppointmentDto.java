package com.sippulse.pet.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sippulse.pet.entity.Appointment;
import com.sippulse.pet.service.EmployeeService;
import com.sippulse.pet.service.PetService;
import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.AbstractRepresentationBuilder;
import com.sippulse.pet.utils.IDto;

/**
 * Representação da entidade {@link Appointment} para retorno da API
 */
@SuppressWarnings("serial")
public class AppointmentDto implements IDto {

    private Long id;
    private PetDto pet;
    private EmployeeDto employee;
    private LocalDateTime dhScheduling;
    private LocalDateTime dhAppointment;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public PetDto getPet() {
        return pet;
    }

    public void setPet(final PetDto pet) {
        this.pet = pet;
    }

    public EmployeeDto getEmployee() {
        return employee;
    }

    public void setEmployee(final EmployeeDto employee) {
        this.employee = employee;
    }

    public LocalDateTime getDhScheduling() {
        return dhScheduling;
    }

    public void setDhScheduling(final LocalDateTime dhScheduling) {
        this.dhScheduling = dhScheduling;
    }

    public LocalDateTime getDhAppointment() {
        return dhAppointment;
    }

    public void setDhAppointment(final LocalDateTime dhAppointment) {
        this.dhAppointment = dhAppointment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Component
    public static final class RepresentationBuilder
            extends AbstractRepresentationBuilder<Appointment, AppointmentDto, Appointment.Builder> {

        @Autowired
        private PetService petService;

        @Autowired
        private EmployeeService employeeService;

        @Autowired
        private PetDto.RepresentationBuilder petDtoBuilder;

        @Autowired
        private EmployeeDto.RepresentationBuilder employeeDtoBuilder;

        public Appointment fromRepresentation(final AppointmentSimplifiedDto representation) {
            if (representation == null) {
                return null;
            }
            return Appointment.Builder.create()
                    .pet(Optional.ofNullable(representation.getPetId())
                            .map(petService::findById)
                            .orElse(null))
                    .employee(Optional.ofNullable(representation.getEmployeeId())
                            .map(employeeService::findById)
                            .orElse(null))
                    .dhAppointment(representation.getDhAppointment())
                    .description(representation.getDescription())
                    .build();
        }

        @Override
        public Appointment fromRepresentation(final AppointmentDto representation, final Appointment.Builder builder) {
            if (representation == null) {
                return null;
            }
            return builder
                    .id(representation.getId())
                    .pet(Optional.ofNullable(representation.getPet())
                            .map(PetDto::getId)
                            .map(petService::findById)
                            .orElse(null))
                    .employee(Optional.ofNullable(representation.getEmployee())
                            .map(EmployeeDto::getId)
                            .map(employeeService::findById)
                            .orElse(null))
                    .dhAppointment(representation.getDhAppointment())
                    .description(representation.getDescription())
                    .build();
        }

        public AppointmentDto toJdbcRepresentation(final AppointmentJdbcDto entity) {
            if (entity == null) {
                return null;
            }
            return AppointmentDto.Builder.create()
                    .id(entity.getId())
                    .pet(Optional.ofNullable(entity.getPetId())
                            .map(petService::findById)
                            .map(petDtoBuilder::toRepresentation)
                            .orElse(null))
                    .employee(Optional.ofNullable(entity.getEmployeeId())
                            .map(employeeService::findById)
                            .map(employeeDtoBuilder::toRepresentation)
                            .orElse(null))
                    .dhScheduling(entity.getDhScheduling())
                    .dhAppointment(entity.getDhAppointment())
                    .description(entity.getDescription())
                    .build();
        }

        public List<AppointmentDto> toJdbcRepresentation(final List<AppointmentJdbcDto> list) {
            return list.stream().map(this::toJdbcRepresentation).collect(Collectors.toList());
        }

        @Override
        public AppointmentDto toRepresentation(final Appointment entity) {
            if (entity == null) {
                return null;
            }
            return AppointmentDto.Builder.create()
                    .id(entity.getId())
                    .pet(petDtoBuilder.toRepresentation(entity.getPet()))
                    .employee(employeeDtoBuilder.toRepresentation(entity.getEmployee()))
                    .dhScheduling(entity.getDhScheduling())
                    .dhAppointment(entity.getDhAppointment())
                    .description(entity.getDescription())
                    .build();
        }
    }

    public static class Builder extends AbstractBuilder<AppointmentDto> {

        public Builder(final AppointmentDto dto) {
            super(dto);
        }

        public static Builder create() {
            return new Builder(new AppointmentDto());
        }

        public static Builder from(final AppointmentDto dto) {
            return new Builder(dto);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder pet(final PetDto pet) {
            item.setPet(pet);
            return this;
        }

        public Builder employee(final EmployeeDto employee) {
            item.setEmployee(employee);
            return this;
        }

        public Builder dhScheduling(final LocalDateTime dhScheduling) {
            item.setDhScheduling(dhScheduling);
            return this;
        }

        public Builder dhAppointment(final LocalDateTime dhAppointment) {
            item.setDhAppointment(dhAppointment);
            return this;
        }

        public Builder description(final String description) {
            item.setDescription(description);
            return this;
        }
    }
}