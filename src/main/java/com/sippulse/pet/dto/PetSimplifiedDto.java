package com.sippulse.pet.dto;

import com.sippulse.pet.entity.Pet;
import com.sippulse.pet.utils.IDto;

/**
 * Representação simplificada da entidade {@link Pet} para cadastro
 */
@SuppressWarnings("serial")
public class PetSimplifiedDto implements IDto {

    private String name;
    private String description;
    private Long clientId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
}