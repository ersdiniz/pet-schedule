package com.sippulse.pet.dto;

import com.sippulse.pet.entity.Client;
import com.sippulse.pet.utils.IDto;

/**
 * Representação simplificada da entidade {@link Client} para cadastro
 */
@SuppressWarnings("serial")
public class ClientSimplifiedDto implements IDto {

    private String name;
    private String cpf;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}