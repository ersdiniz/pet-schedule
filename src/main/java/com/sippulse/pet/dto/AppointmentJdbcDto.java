package com.sippulse.pet.dto;

import java.time.LocalDateTime;

import org.springframework.jdbc.core.JdbcTemplate;

import com.sippulse.pet.entity.Appointment;
import com.sippulse.pet.utils.IDto;

/**
 * Representação da entidade {@link Appointment} extraída via
 * {@link JdbcTemplate}
 */
@SuppressWarnings("serial")
public class AppointmentJdbcDto implements IDto {

    private Long id;
    private Long petId;
    private Long employeeId;
    private LocalDateTime dhScheduling;
    private LocalDateTime dhAppointment;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getPetId() {
        return petId;
    }

    public void setPetId(final Long petId) {
        this.petId = petId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(final Long employeeId) {
        this.employeeId = employeeId;
    }

    public LocalDateTime getDhScheduling() {
        return dhScheduling;
    }

    public void setDhScheduling(final LocalDateTime dhScheduling) {
        this.dhScheduling = dhScheduling;
    }

    public LocalDateTime getDhAppointment() {
        return dhAppointment;
    }

    public void setDhAppointment(final LocalDateTime dhAppointment) {
        this.dhAppointment = dhAppointment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}