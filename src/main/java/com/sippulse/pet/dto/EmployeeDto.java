package com.sippulse.pet.dto;

import org.springframework.stereotype.Component;

import com.sippulse.pet.entity.Employee;
import com.sippulse.pet.enums.TypeEmployee;
import com.sippulse.pet.utils.AbstractBuilder;
import com.sippulse.pet.utils.AbstractRepresentationBuilder;
import com.sippulse.pet.utils.IDto;

/**
 * Representação da entidade {@link Employee} para retorno da API
 */
@SuppressWarnings("serial")
public class EmployeeDto implements IDto {

    private Long id;
    private String name;
    private String cpf;
    private String username;
    private String password;
    private TypeEmployee type;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public TypeEmployee getType() {
        return type;
    }

    public void setType(TypeEmployee type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Component
    public static final class RepresentationBuilder extends AbstractRepresentationBuilder<Employee, EmployeeDto, Employee.Builder> {

        public Employee fromRepresentation(final EmployeeSimplifiedDto representation) {
            if (representation == null) {
                return null;
            }
            return Employee.Builder.create()
                    .name(representation.getName())
                    .cpf(representation.getCpf())
                    .username(representation.getUsername())
                    .password(representation.getPassword())
                    .type(representation.getType())
                    .build();
        }

        @Override
        public Employee fromRepresentation(final EmployeeDto representation, final Employee.Builder builder) {
            if (representation == null) {
                return null;
            }
            return builder
                    .id(representation.getId())
                    .name(representation.getName())
                    .cpf(representation.getCpf())
                    .username(representation.getUsername())
                    .password(representation.getPassword())
                    .type(representation.getType())
                    .build();
        }

        @Override
        public EmployeeDto toRepresentation(final Employee entity) {
            if (entity == null) {
                return null;
            }
            return EmployeeDto.Builder.create()
                    .id(entity.getId())
                    .name(entity.getName())
                    .cpf(entity.getCpf())
                    .username(entity.getUsername())
                    .type(entity.getType())
                    .build();
        }
    }

    public static class Builder extends AbstractBuilder<EmployeeDto> {

        public Builder(final EmployeeDto dto) {
            super(dto);
        }

        public static Builder create() {
            return new Builder(new EmployeeDto());
        }

        public static Builder from(final EmployeeDto dto) {
            return new Builder(dto);
        }

        public Builder id(final Long id) {
            item.setId(id);
            return this;
        }

        public Builder name(final String name) {
            item.setName(name);
            return this;
        }

        public Builder cpf(final String cpf) {
            item.setCpf(cpf);
            return this;
        }

        public Builder username(final String username) {
            item.setUsername(username);
            return this;
        }

        public Builder password(final String password) {
            item.setPassword(password);
            return this;
        }

        public Builder type(final TypeEmployee type) {
            item.setType(type);
            return this;
        }
    }
}