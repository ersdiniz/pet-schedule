package com.sippulse.pet.security;

import org.springframework.util.DigestUtils;

public class Encryptor {

    private Encryptor() {
    }

    /**
     * Cria um hash <a href="https://www.ietf.org/rfc/rfc1321.txt">MD5</a> baseado
     * no username e na data e hora atuais
     * 
     * @param username
     * @return hash MD5
     */
    public static String encrypt(final String username) {
        final String key = username + "#" + System.currentTimeMillis();
        return DigestUtils.md5DigestAsHex(key.getBytes()).toUpperCase();
    }
}