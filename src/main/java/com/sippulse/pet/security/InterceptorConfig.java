package com.sippulse.pet.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sippulse.pet.exception.PetScheduleBadRequestException;
import com.sippulse.pet.exception.PetScheduleForbiddenException;
import com.sippulse.pet.service.UserSessionService;

/**
 * Interceptor de validação de usuário autenticado
 */
@Component
public class InterceptorConfig extends HandlerInterceptorAdapter {

    @Autowired
    private UserSessionService userSessionService;

    @Override
    public boolean preHandle(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final Object handler)
            throws Exception {
        final String authorization = request.getHeader("Authorization");
        if (authorization == null || authorization.trim().isEmpty()) {
            throw new PetScheduleBadRequestException("Authorization not found on header");
        }

        final String token = authorization.replace("Bearer ", "");
        if (token.trim().isEmpty()) {
            throw new PetScheduleBadRequestException("Authorization not contain a valid token");
        }

        if (!userSessionService.isValid(token)) {
            throw new PetScheduleForbiddenException("Forbidden");
        }
        return true;
    }
}