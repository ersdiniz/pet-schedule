# Pet Schedule com Spring Boot

# Tecnologias

[Java 1.8](https://docs.oracle.com/javase/8/docs/)<br/>
[Maven](https://maven.apache.org/)<br/>
[Spring Boot](https://spring.io/projects/spring-boot)<br/>
[RESTful](https://pt.wikipedia.org/wiki/REST)<br/>
[MySQL](https://dev.mysql.com/doc/)<br/>
[Springfox Swagger](https://springfox.github.io/springfox/docs/current/)<br/>
[Liquibase](https://www.liquibase.org/documentation/index.html)<br/>
[JUnit](https://junit.org/junit5/docs/current/user-guide/)<br/>


#### Execução

**Nota: Parte-se do princípio de que o banco de dados MySQL 8+ já está instalado localmente.**

Criar o usuário do sistema no banco de dados (Apenas caso ainda não tenha sido criado):

`CREATE USER 'petschedule-sys'@'localhost' IDENTIFIED BY 'a1b2c3d4e5';`


Conceder as permissões ao usuário do sistema (Apenas caso ainda não tenha sido concedido):

`GRANT ALL PRIVILEGES ON * . * TO 'petschedule-sys'@'localhost';`


Criar o database (Apenas caso ainda não tenha sido criado):

`CREATE DATABASE petschedule;`


Executar o sistema:

Windows: `run.bat`<br/>
Outros: `sh run.sh`


#### Testes

Windows: `run-test.bat`<br/>
Outros: `sh run-test.sh`


#### Documentação

Executar a aplicação e acessar: [http://localhost:8083/petschedule/swagger-ui.html](http://localhost:8083/petschedule/swagger-ui.html)


#### Orientações

O sistema utiliza o liquibase para controle do banco. Todas as tabelas e o usuário inicial para utilizar a aplicação serão criados ao iniciar o sistema.

As APIs foram implementadas no padrão RESTful. Toda a sua documentação foi criada através do swagger e pode ser conferida [aqui](http://localhost:8083/petschedule/swagger-ui.html), enquanto a aplicação estiver rodando.

Para utilizar as APIs, é necessário previamente efetuar login no sistema no _endpoint_ `/petschedule/login/signin`, passando no _body_ o `username` `admin` e o `password` `admin`. É retornado o _token_ que deverá ser passado no header das demais requisições do sistema com o seguinte formato: nome `Authorization` e o valor `Bearer <token>`.<br/>
**Nota: A API** `/petschedule/api/v1/appointments/cpf/{cpf}` **foge a esta regra e não necessita da authorization no header.**


#### TODO

- Incluir docker.
- Migrar padrão de login para OAuth2.
- Aumentar a cobertuda de testes.